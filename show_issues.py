#!/usr/bin/env python3


import argparse
import textwrap
import sys
import os
from datetime import date, timedelta
from fractions import Fraction
from dateutil.relativedelta import relativedelta
import importlib
redmine_api = importlib.import_module("create-standup-issues")


REDMINE_URL = os.getenv('REDMINE_URL')

REDMINE_API_KEY = os.getenv('REDMINE_API_KEY')


class IssueManager:
    def __init__(self):
        self.issues = {}
        self.project = Project()

    def add(self, issue_or_project):
        if type(issue_or_project) is Issue:
            issue = issue_or_project
            if issue.issue_id not in self.issues:
                self.issues[issue.issue_id] = Issue(issue.issue_id)
            self.issues[issue.issue_id].add_hours(issue.hours)
        elif type(issue_or_project) is Project:
            project = issue_or_project
            self.project.add_hours(project.hours)
        else:
            raise Exception(f"Invalid type: {type(issue_or_project)}")

    def __str__(self):
        result = ""
        if self.project.hours > 0:
            result += str(self.project) + "\n"
        for issue in self.issues.values():
            result += str(issue) + "\n"
        return result


class Issue:
    def __init__(self, issue_id, hours = 0):
        self.issue_id = issue_id
        self.hours = hours

    def add_hours(self, hours):
        self.hours += float(Fraction(hours))

    def __str__(self):
        return f"##{self.issue_id}: {self.hours}h"


class Project:
    def __init__(self, hours = 0):
        self.hours = hours

    def add_hours(self, hours):
        self.hours += float(Fraction(hours))

    def __str__(self):
        return f"Project: {self.hours}h"


def get_time_entries(project, year, month, month_count = 1, category_id = None):
    start_date = date(year, month, 1)
    end_date = start_date + relativedelta(months=month_count) - timedelta(days=1)
    url = (
        f"projects/{project}/time_entries.json?"
        "limit=100"
        "&set_filter=1"
        "&f[]=spent_on"
        "&op[spent_on]=><"
        f"&v[spent_on][]={start_date.strftime('%Y-%m-%d')}"
        f"&v[spent_on][]={end_date.strftime('%Y-%m-%d')}"
    )
    if category_id is not None:
        if category_id != "none":
            url += (
                "&f[]=issue.category_id"
                "&op[issue.category_id]=="
                f"&v[issue.category_id][]={category_id}"
            )
        else:
            url += (
                "&f[]=issue.category_id"
                "&op[issue.category_id]=!*"
            )

    result = redmine_api.redmine_api(url)
    return result["time_entries"]


def summarize_time_entries(project, year, month, month_count = 1, category_id = None):
    time_entries = get_time_entries(project, year, month, month_count, category_id)

    manager = IssueManager()
    for time_entry in time_entries:
        if "issue" in time_entry:
            manager.add(Issue(time_entry["issue"]["id"], time_entry["hours"]))
        else:
            manager.add(Project(time_entry["hours"]))

    return manager


def main(
    project_name: str,
    year: int,
    month: int,
    month_count: int = 1,
    category_id: str = None,
):
    if not REDMINE_URL:
        return False

    if not REDMINE_API_KEY:
        return False

    manager = summarize_time_entries(project_name, year, month, month_count, category_id)
    print(manager)

    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Show issues and hours in the months.",
        epilog=textwrap.dedent(
            """
            # You need to set the redmine key (visit /my/account)
            $ export REDMINE_API_KEY="xxxxxxxx"
            $ export REDMINE_URL="https://redmine.example.com"

            # Example
            $ python3 show_issues.py project_name 202101
            $ python3 show_issues.py project_name 202101 -m 3 -c 15
            """
        )
    )

    parser.add_argument("project_name", type=str,
        help="Project name. You can check it in URI (http...com/projects/{project_name}/time_entries).")
    parser.add_argument("year_and_month", type=str, help="YYYYMM. Ex. 202206.")
    parser.add_argument("-m", "--months", type=int, default=1, help="Specify how many months to get data. Default: 1.")
    parser.add_argument("-c", "--categoryid", type=str,
        help="Specify category id to filter data. You can also use 'none' to filter only none category.)")

    args = parser.parse_args()

    has_succeeded = main(
        args.project_name,
        int(args.year_and_month[:4]),
        int(args.year_and_month[-2:]),
        args.months,
        args.categoryid,
    )

    if not has_succeeded:
        parser.print_help()
        sys.exit(-1)
    sys.exit(0)
