Redmine Scripts
===============

redmine-tools maintains a handful of niche tools to create, update and
manipulate Redmine tickets, used internally in ClearCode Inc.

List of Scripts
---------------

### create-standup-issues.py

* Create daily standup tickets for the specified month. This skips holidays and already existing dates.
* Need [jpholiday](https://github.com/Lalcs/jpholiday) module.
    * `$ python3 -m pip install jpholiday`
* Need [python-dateutil](https://pypi.org/project/python-dateutil/) module.
    * `$ python3 -m pip install python-dateutil`

### show_issues.py

* Show issues and hours in the months.

License
-------

MIT License
