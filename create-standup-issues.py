#!/usr/bin/env python3

"""
Create Redmine tickets for daily standup

USAGE

  # Set the redmine key (visit /my/account)
  $ export REDMINE_API_KEY="xxxxxxxx"
  $ export REDMINE_URL="https://redmine.example.com"

  # Install "jpholiday" module to exclude special holidays of Japan.
  # (Now this feature is always ON, so it must be installed.
  # In the future, the feature of this script should be generalized.)
  $ python3 -m pip install jpholiday

  # Run the script with an argument (YYYYMM)
  $ python3 create-standup-issues.py 202101
"""

import json
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
import sys
import os
import calendar
import urllib
from urllib.request import Request, urlopen

try:
    import jpholiday
except ImportError:
    pass

#
# CONSTANT

REDMINE_URL = os.getenv('REDMINE_URL')

REDMINE_API_KEY = os.getenv('REDMINE_API_KEY')

DRYRUN = 0  # Set 1 for debug

#
# Redmine API

def redmine_api(target, params=None):
    """
    `params`: specify data for POST, otherwise use only the target for GET.
    """
    url = REDMINE_URL + '/' + target
    data = None
    if params:
        data = json.dumps({'issue': params}).encode()
        print(data)
        if DRYRUN: return

    try:
        resp = urlopen(Request(url, data=data, headers={
            'X-Redmine-API-Key': REDMINE_API_KEY,
            'Content-Type': 'application/json',
        }))
    except Exception as e:
        print(json.loads(e.read().decode()))
        raise

    return json.loads(resp.read().decode())

def strftime_for_redmine(date_):
    return date_.strftime('%Y-%m-%d')

class TargetIssue:
    def __init__(self, need_create_callback, get_issue_callback):
        self.need_create_callback = need_create_callback
        self.get_issue_callback = get_issue_callback

    def need_create(self, date_):
        return self.need_create_callback(date_)
    def get_issue(self, date_):
        return self.get_issue_callback(date_)

def create_issue(date_, target_issues, existing_subject_set):
    for target_issue in target_issues:
        if not target_issue.need_create(date_):
            continue
        issue = target_issue.get_issue(date_)
        subject = issue['subject']
        if subject in existing_subject_set:
            continue
        try:
            redmine_api('issues.json', issue)
        except urllib.error.HTTPError:
            print(f"The creation of \"{subject}\" may be failed. Continue creating issue.")
            pass
        except Exception:
            raise

def get_existing_subject_set(year, month):
    """
    Returns
    -------
    Set of subject in the target month.
    """
    start_date = date(year, month, 1)
    end_date = start_date + relativedelta(months=1) - timedelta(days=1)
    subject_set = set()
    limit = 100
    max_offset = 5 * limit

    for offset in range(0, max_offset, limit):
        url = (
            "issues.json?"
            f"limit={limit}"
            f"&offset={offset}"
            "&set_filter=1"
            "&f[]=due_date"
            "&op[due_date]=><"
            f"&v[due_date][]={strftime_for_redmine(start_date)}"
            f"&v[due_date][]={strftime_for_redmine(end_date)}"
        )
        result = redmine_api(url)
        for issue in result["issues"]:
            subject_set.add(issue["subject"])
        if len(result["issues"]) < 100:
            break

    return subject_set

def get_dates_to_process(
    year,
    month,
    exclude_holidays = True,
    exclude_special_jp_holidays = True
):
    """
    Parameters
    ----------
    year: int
    month: int
    exclude_holidays: bool, default True
        True for excluding Saturday and Sunday.
    exclude_special_jp_holidays: bool, default True
        True for excluding special holidays of Japan.
        Need to install 'jpholiday' to use this feature.

    Returns
    -------
    dates to process: generator of date
    """
    if exclude_special_jp_holidays:
        if "jpholiday" not in sys.modules:
            raise Exception(
                "Need to install 'jpholiday' module to exclude special"
                " holidays of Japan."
            )

    cal = calendar.Calendar()
    for date_ in cal.itermonthdates(year, month):
        if date_.month != month:
            continue
        if exclude_holidays and is_holidays(date_):
            continue
        if exclude_special_jp_holidays and is_special_jp_holidays(date_):
            continue
        yield date_

def is_holidays(date_):
    return 5 <= date_.weekday()  # Skip Saturday=5 and Sunday=6

def is_special_jp_holidays(date_):
    return jpholiday.is_holiday(date_) 

#
# Main


def get_target_issues():
    return [
        TargetIssue(
            lambda date_: True,
            lambda date_:
            {
                'project_id': 95,
                'tracker_id': 7,
                'subject': '%s  朝会' % strftime_for_redmine(date_),
                'status_id': 5,
                'priority_id': 4,
                'start_date': strftime_for_redmine(date_),
                'due_date': strftime_for_redmine(date_),
            }
        )
    ]

def usage(err):
    print(err)
    print(__doc__, file=sys.stderr)

def main(argv):
    if len(argv) != 2:
        usage("ERR missing argument YYYYMM")
        return -1

    if not REDMINE_URL:
        usage("ERR missing REDMINE_URL")
        return -1

    if not REDMINE_API_KEY:
        usage("ERR missing REDMINE_API_KEY")
        return -1

    y = int(argv[1][:4])
    m = int(argv[1][-2:])

    msg = "Create issues for %04i/%02i on '%s'? [y/n] " % (y, m, REDMINE_URL)
    if input(msg) not in ('yes', 'y'):
        print("aborted!")
        return -1

    # TODO add main argument to manage filter options of "get_dates_to_process"
    dates = list(get_dates_to_process(y, m))
    if not dates:
        print("There is no date to process. Finish.")
        return 0

    target_issues = get_target_issues()
    existing_subject_set = get_existing_subject_set(y, m)

    for date_ in dates:
        create_issue(date_, target_issues, existing_subject_set)
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
